#!/bin/bash

WORKDIR="/home/$USER/assignment"
CRED="$WORKDIR/.cred"
LOGFILE="$WORKDIR/prog1.log"

if [ $# -eq 2 ];then
       IN_FILE=$1
       SYM_KEY=$2
else
        echo "Script expects 2 parameters: <Encrypted File> <Encrypted Symmetric Key>"
        exit 1
fi

export FORTANIX_API_ENDPOINT=https://sdkms.fortanix.com

sdkms-cli user-login $(cat $CRED | base64 -d) > $LOGFILE 

sleep 5

applogin()
{
        appid=$1
        sdkms-cli app-login --api-key $(sdkms-cli get-app-api-key --app-id $appid)
}

appname="app2"

appid=$(sdkms-cli list-apps | grep $appname | awk '{print $1}')

applogin $appid  #App2 Login

rsakey1=$(sdkms-cli list-keys | grep rsakey | awk '{print $1}')

sdkms-cli decrypt --kid $rsakey1  --name rsakey --in "$SYM_KEY" --alg RSA --out sym.key

symkeyid=$(sdkms-cli list-keys | grep symkey | awk '{print $1}')

[[ -z $symkeyid ]] && symkeyid=$(sdkms-cli import-key --in sym.key --name symkey --description "SYMMETRIC KEY" --exportable --force --obj-type AES)

sdkms-cli decrypt --kid $symkeyid --name symkey --in "$IN_FILE" --alg AES --mode KWP --out decrypted_file.txt && rm sym.key

sdkms-cli app-logout

sdkms-cli user-logout
