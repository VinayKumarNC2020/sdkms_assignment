#!/bin/bash

WORKDIR="/home/$USER/assignment"
CRED="$WORKDIR/.cred"
LOGFILE="$WORKDIR/prog1.log"

if [ $# -eq 1 ];then
       IN_FILE=$1
else
        echo "Please provide file name to be encrypted"
        exit 1
fi

export FORTANIX_API_ENDPOINT=https://sdkms.fortanix.com

sdkms-cli user-login $(cat $CRED | base64 -d) > $LOGFILE 

sleep 10

# This Function takes group name and group description as parameters and verify whether group exists, if group exists then
# returns group id, if not creates group and returns group id
creategroup()
{
        grpname=$1
        grpdesc=$2
        grpid=$(sdkms-cli list-groups | grep $grpname | awk '{print $1}')
        if [ -z $grpid ];then
                echo "Group $grpname does'nt exists" >> $LOGFILE
                gid=$(sdkms-cli create-group --name "$grpname" --description "$grpdesc")
                echo $grpid
        else
                echo "Group $grpname already exists" >> $LOGFILE
                echo $grpid
        fi
}

# This Function takes App name, App description, App group and default group as parameters and verify whether group exists, #if App exists then returns App id, if not creates App and returns App id
createapp()
{
        appname=$1
        appdesc=$2
        appgrp=$3
        appdefgrp=$4
        appid=$(sdkms-cli list-apps | grep $appname | awk '{print $1}')
        if [ -z $appid ];then
                echo "App $appname does'nt exists" >> $LOGFILE
                appid=$(sdkms-cli create-app --name "$appname" --description "$appdesc" --groups "$appgrp" --default-group "$appdefgrp")
                echo $appid
        else
                echo "App $appname already exists" >> $LOGFILE
                echo $appid
        fi
}

# This function takes App id as parameter and creates a session for the App to perform Cryptographic Operations
applogin()
{
        appid=$1
        sdkms-cli app-login --api-key $(sdkms-cli get-app-api-key --app-id $appid)
}

grp1id=$(creategroup "grp1" "Group 1")
grp2id=$(creategroup "grp2" "Group 2")

app1id=$(createapp "app1" "Application 1" $grp1id $grp1id)
app2id=$(createapp "app2" "Application 2" $grp2id $grp2id)

aeskey1=$(sdkms-cli list-keys | grep aeskey | awk '{print $1}')
[[ -z $aeskey1 ]] && aeskey1=$(sdkms-cli create-key --obj-type AES --name "aeskey" --key-size 256 --description "Symmetric Key" --group-id "$grp1id" --exportable  --force)

rsakey1=$(sdkms-cli list-keys | grep rsakey | awk '{print $1}')
[[ -z $rsakey1 ]] && rsakey1=$( sdkms-cli create-key --obj-type RSA --name "rsakey" --key-size 2048 --description "ASymmetric Key" --group-id "$grp2id" --exportable  --force)

applogin $app2id  #App2 Login

sdkms-cli export-object --kid $rsakey1 --name rsakey > key.pem 

openssl rsa -in key.pem -pubout > key.pub && rm key.pem

sdkms-cli app-logout #App2 Logout

applogin $app1id  #App1 Login

sdkms-cli encrypt --kid $aeskey1 --in "$IN_FILE" --name aeskey --alg AES --out "${IN_FILE}_enrypted" --mode KWP

sdkms-cli export-object --kid $aeskey1 --name aeskey > $WORKDIR/sym.key

rsapubkeyid=$(sdkms-cli list-keys | grep rsapubkey | awk '{print $1}')

[[ -z $rsapubkeyid ]] && rsapubkeyid=$(sdkms-cli import-key --in key.pub --name rsapubkey --description "RSA PUB KEY" --exportable --force --obj-type RSA)

sdkms-cli encrypt --kid $rsapubkeyid  --name rsapubkey --in sym.key --alg RSA --out symkey.encr && rm sym.key

sdkms-cli app-logout #App1 Logout

sdkms-cli user-logout
