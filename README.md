## SDKMS_ASSIGNMENT

#Requirements
--------------

One program will do the follow.
---------------------------------

Create 2 groups on SDKMS grp1, grp2

Create an app on each group app1, app2

Create an aes key in grp1 and rsa key in grp2

Encrypt the data “Hello World” using aes key

Encrypt the  aes key with the rsa public key

Pass the cipher text and encrypted aes key

Second program will do the following:
-------------------------------------
Accept the cipher text and encrypted Aes Key

Decrypt the cipher text and display the original message “Hello World”


--------------------------------------------------------------------------------
Script : encrypt_file.sh and decrypt_file.sh

Summary: For Scripts to work, need below setup 

* Create a directory assignment /home/$USER/assignment
* Create credentials file .cred with 400 permissions.
   File contents :  echo -n "--username testuser@gmail.com --password youraccountpwd" | base64 > .cred 
* copy the scripts encrypt_file.sh and decrypt_file.sh to /home/testuser/assignment
* Provide Execute Permissions to the scripts:
   chmod 700 *.sh
* Create dummy file hello.txt with contents Hello World :  echo "Hello World" > hello.txt
* ./encrypt_file.sh hello.txt

    Script creates 2 files : symkey.encr and hello.txt_enrypted
    symkey.encr is an encrypted symmetric key which has been encrypted using RSA public key.
* Execute ./decrypt_file.sh  hello.txt_enrypted symkey.encr
* Creates decrypted version of file hello.txt_enrypted ---> decrypted_file.txt

Note : Script uses sdkms-cli commands to perform all operations, when script login using user credentials, 
       it creates a .token file in /home/testuser/.token which consists of user-token returned by Session API and app login 
       would update this .token file with app-token. 
       At the end of the script using log-out commmands would remove the token details from .token file.

Options to Explore:
---------------------
* Store sdkms account login credentials in Vault 
* Configure certificate login for Apps
